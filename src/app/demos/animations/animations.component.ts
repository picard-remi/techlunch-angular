import {
  Component,
  Input,
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/core';

import { Speakers } from './animations.service';

@Component({
  selector: 'animations',
  templateUrl: './animations.component.html',
  styleUrls: ['./animations.component.css'],
  providers: [Speakers]
})
export class AnimationsComponent {
  constructor(private speakers: Speakers) { }
}
