import {Component, OnInit} from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'routing',
    template: '<router-outlet></router-outlet>'
})
export class RoutingComponent  { }