import {Slide, ImageFragment, ListFragment, Speaker} from "../slide.model";
import {TITRE1} from "../slide.constantes";

export const SLIDE_1_1: Slide = new Slide();
SLIDE_1_1.title = TITRE1;
SLIDE_1_1.speaker = Speaker.A;

let list1 = new ListFragment();
list1.className='historique vertical-center no-list-style-type';
list1.pushContent('<div class="row"><div class="col-sm-4">14 Septembre 2014</div><div class="col-sm-8">Initial Commit</div></div>');
list1.pushContent('<div class="row"><div class="col-sm-4">10 Février 2015</div><div class="col-sm-8">Première version alpha</div></div>');
list1.pushContent('<div class="row"><div class="col-sm-4">15 Décembre 2015</div><div class="col-sm-8">Dernière Alpha (numéro 55) -> Beta 0</div></div>');
list1.pushContent('<div class="row"><div class="col-sm-4">28 Avril 2016</div><div class="col-sm-8"> Dernière Beta (numéro 17)</div></div>');
list1.pushContent('<div class="row"><div class="col-sm-4">02 Mai 2016</div><div class="col-sm-8">Première Release Candidate</div></div>');
list1.pushContent('<div class="row"><div class="col-sm-4">31 Août 2016</div><div class="col-sm-8">Dernière (vrai) Release Candidate (numéro 6)</div></div>');
list1.pushContent('<div class="row"><div class="col-sm-4">14 Septembre 2016</div><div class="col-sm-8">Version Finale</div></div>');
list1.hideOnStep=1;
SLIDE_1_1.pushFragments(list1);

let tendance = new ImageFragment();
tendance.className='tendance vertical-center';
tendance.content='/src/images/tendance.png';
tendance.stepNumber=1;
SLIDE_1_1.pushFragments(tendance);
