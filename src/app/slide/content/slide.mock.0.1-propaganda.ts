import {Slide, SubTitleFragment, ImageFragment, ListFragment} from "../slide.model";

export const SLIDE_0_1_1: Slide = new Slide();

let subTitle1 = new SubTitleFragment();
subTitle1.content=`Le contexte`;
subTitle1.hideOnStep= 1;
SLIDE_0_1_1.pushFragments(subTitle1);

let list1 = new ListFragment();
list1.className='context no-list-style-type';
list1.pushContent('Participation aux développements de la battle Nantaise 2016');
list1.pushContent('<a href="http://codeandplay.date">www.codeandplay.date</a>');
list1.hideOnStep=1;
SLIDE_0_1_1.pushFragments(list1);

let img1 = new ImageFragment();
img1.content = '/src/images/battle.png';
img1.className = 'battle';
img1.stepNumber=1;
img1.hideOnStep=2;
SLIDE_0_1_1.pushFragments(img1);

let subTitle2 = new SubTitleFragment();
subTitle2.content=`Le contexte`;
subTitle2.stepNumber=2;
SLIDE_0_1_1.pushFragments(subTitle2);

let list2 = new ListFragment();
list2.className='context no-list-style-type';
list2.pushContent('Début des développements sur Angular2 depuis Juillet 2016');
list2.pushContent('Développement de 2 applications :');
list2.pushContent(`<ul> 
    <li>Administration - gestion de la Battle, refonte AngularJS Vers Angular2, Bootstrap4</li>
    <li>Replay - rendu graphique des matchs, composants Polymer</li>
</ul>`);
list2.stepNumber=2;
list2.hideOnStep=3;
SLIDE_0_1_1.pushFragments(list2);


let list3 = new ListFragment();
list3.className='context no-list-style-type';
list3.pushContent('Début des développements sur la version RC4');
list3.pushContent('Durant quelques semaines, quelques doutes sur le Framework');
list3.pushContent(`<ul> 
    <li>Non stabilité de l'API</li>
    <li>Temps de montée en compétence</li>
    <li>Manque de documentation sur les outils de build</li>
    <li>Peu de sujets, d'aide sur le Web</li>
</ul>`);
list3.stepNumber=3;
SLIDE_0_1_1.pushFragments(list3);