import {Slide, SubTitleFragment, CodeFragment, LinkFragment, Speaker} from "../slide.model";
import {TITRE5} from "../slide.constantes";

export const SLIDE_4_3: Slide = new Slide();
SLIDE_4_3.title = TITRE5;
SLIDE_4_3.speaker = Speaker.A;

let subTitle = new SubTitleFragment();
subTitle.content = 'SystemJS';
SLIDE_4_3.pushFragments(subTitle);

let code1 = new CodeFragment();
code1.content = `<pre>
    <code class="html highlight">&lt;!-- index.html --&gt;
&lt;!DOCTYPE html&gt;
&lt;html&gt;
  &lt;head&gt;
    ...
  &lt;/head&gt;

  &lt;body&gt;
    &lt;my-app&gt;Loading...&lt;/my-app&gt;
    
    &lt;script src="../../../node_modules/systemjs/dist/system.src.js"&gt;&lt;/script&gt;
    &lt;script src="systemjs.conf.js"&gt;&lt;/script&gt;
    &lt;script&gt;
        document.addEventListener('WebComponentsReady', function() {
          System.import('app').catch(function(err){ console.error(err); });
        });
    &lt;/script&gt;
  &lt;/body&gt;
&lt;/html&gt;</code>
</pre>
`;
code1.hideOnStep=1;
SLIDE_4_3.pushFragments(code1);

let link1 = new LinkFragment();
link1.content='SystemJS';
link1.href='https://github.com/systemjs/systemjs';
link1.hideOnStep=1;
SLIDE_4_3.pushFragments(link1);

let code2 = new CodeFragment();
code2.content = `<pre>
    <code class="javascript highlight">// systemjs.conf.js
(function(global) {
  var map = {
    'app': 'src/main/frontend/tmp/app'
  };
  var packages = {
    'app':                        { main: 'main.js',  defaultExtension: 'js' },
    'rxjs':                       { defaultExtension: 'js' },
    'symbol-observable':          { main: 'index.js', defaultExtension: 'js' }
  };
  var npmPackages = [ '@angular', 'rxjs', 'symbol-observable' ];
  var ngPackageNames = [ 'common', 'compiler', 'core', 'platform-browser', 'platform-browser-dynamic', 'router'];

  npmPackages.forEach(function (pkgName) {map[pkgName] = 'node_modules/' + pkgName; });
  ngPackageNames.forEach(function(pkgName) { packages['@angular/'+pkgName] = { main: 'bundles/' + pkgName + '.umd.js', defaultExtension: 'js' }; });

  var config = { map: map, packages: packages};

  if (global.filterSystemConfig) { global.filterSystemConfig(config); }
  System.config(config);
})(this);</code>
</pre>
`;
code2.stepNumber=1;
SLIDE_4_3.pushFragments(code2);

