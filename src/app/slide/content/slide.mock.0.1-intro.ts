import {Slide, ImageFragment, HTMLFragment} from "../slide.model";

export const SLIDE_0_1: Slide = new Slide();
SLIDE_0_1.title = 'Angular 2.0';

let img1 = new ImageFragment();
img1.content = '/src/images/angular.svg';
img1.className = 'logo-angular';
SLIDE_0_1.pushFragments(img1);

let speaker1 = new HTMLFragment();
speaker1.content = `<p>Rémi PICARD</p> <p>Agence 156</p> <a href="mailto:remi.picard@soprasteria.com">remi.picard@soprasteria.com</a>`;

let speaker2 = new HTMLFragment();
speaker2.content = `<p>Antoine CARON</p> <p>Agence 136</p> <a href="mailto:antoine.caron@soprasteria.com">antoine.caron@soprasteria.com</a>`;

let random: number = Math.random();

if(random <= 0.5){
    speaker1.className = 'speaker left';
    speaker2.className = 'speaker right';
    SLIDE_0_1.pushFragments(speaker1);
    SLIDE_0_1.pushFragments(speaker2);
} else {
    speaker2.className = 'speaker left';
    speaker1.className = 'speaker right';
    SLIDE_0_1.pushFragments(speaker2);
    SLIDE_0_1.pushFragments(speaker1);
}


