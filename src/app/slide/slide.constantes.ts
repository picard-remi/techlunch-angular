export const TITRE1 = "Présentation générale";
export const TITRE2 = "AngularJS vers Angular2";
export const TITRE3 = "ECMAScript / Typescript";
export const TITRE4 = "Les Concepts";
export const TITRE5 = "Les Outils";
export const TITRE6 = "Retours d'expérience";
