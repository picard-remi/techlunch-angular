@ECHO OFF

::
:: Projet BATTLE Admin-ihm
::
:: Script de démarrage de commande dans le répertoire courant
::

ECHO ==============================
ECHO  Demarrage de la CMD NPM START
ECHO ==============================

SET CURRENT_DIR=%~dp0

cd %CURRENT_DIR%
D:

START cmd.exe
